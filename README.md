Controles P1:
-A: movimiento a la izquierda
-D: movimiento a la derecha
-W: salto
-J: bloqueo
-K: espadazo
-L: lanzar kunai
Controles P2:
-Left Arrow: movimiento a la izquierda
-Right Arrow: movimiento a la derecha
-Up Arrow: salto
-B: bloqueo
-N: espadazo
-M: lanzar kunai
Risketos Addicionals:
-Animator amb Frame by Frame
-Selección de niveles