﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D rb2; //para referirme al rigidbody 
    ParticleSystem ps; //para referirme al sistema de partículas
    public Enemy enemy; //para referirme al enemigo 
    public GameObject kunai; //para referirme al kunai 
    Animator anim; //para referirme al animator 
    float dirX = 0f; //se le asigna el valor de la velocidad horizontal
    public float vel; //velocidad del jugador
    public float jumpForce; //fuerza del salto del jugador
    bool isGrounded; //boolean para saber si está en el suelo
    bool facingRight = true; //para saber si está mirando a la derecha
    bool isDefending;    //para saber si está bloqueando
    public float maxHp; //max hp
    public float currentHp; //hp actual
    public int swordDamage; //damage de la espada
    public int kunaiDamage; //damage del kunai
    int comboStatus = 0; //estado del combo
    public int id; //id del player
    Vector2 localScale; //escala local

    //delegados y eventos
    public delegate void _OnDamaged(int i, float dmg);
    public static event _OnDamaged OnDamaged;

    void DamageTaken(float amount)
    {
        currentHp -= amount;
        if (OnDamaged != null){
            OnDamaged(id, amount);
        }
        if (currentHp <= 0)
        {
            //te mueres o algo
            print("te has muerto oh no sad");
            ps.Play();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>(); //asigno animator a anim
        rb2 = GetComponent<Rigidbody2D>(); //asigno rigidbody a rb2
        ps = this.transform.GetChild(0).GetComponent<ParticleSystem>(); //asigno ParticleSystem a ps
        localScale = transform.localScale; //asigno localScale a localscale
    }

    // Update is called once per frame
    void Update()
    {
        SetAnimationState();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "Ground") //solo si colisiona con el suelo puede saltar
        {
            isGrounded = true;
            print("puedo saltar");
        }
    }

    //si no bloquea y colisiona con espada o kunai, recibe daño y un knockback
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Enemy") 
        {
            if (isDefending)
            {
                rb2.AddForce(Vector2.right * 1000f);
            }
            else
            {
                DamageTaken(swordDamage);
                print(currentHp);
                rb2.AddForce(Vector2.right * 1000f);
            }

        }
        if (collision.transform.tag == "Kunai") 
        {
            if (isDefending)
            {
                rb2.AddForce(Vector2.right * 1000f);
            }
            else
            {
                DamageTaken(kunaiDamage);
                print(currentHp);
                Destroy(collision.gameObject);
                rb2.AddForce(Vector2.right * 1000f);
            }
        }
    }

    void LateUpdate()
    {
        Facing();
    }
    //comprueba hacia que lado está mirando
    void Facing()
    {
        if (dirX > 0){
            facingRight = true;
        }else if (dirX < 0)
        {
            facingRight = false;
        }
        if ((facingRight && localScale.x < 0) || (!facingRight && localScale.x > 0))
        {
            localScale.x *= -1;
        }
        transform.localScale = localScale;
    }

    //inputs con movimiento, ataques y corrutinas
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.A) && !isDefending)
        {
            dirX = -vel;
            rb2.velocity = new Vector2(-vel, rb2.velocity.y);
        }
        else if (Input.GetKey(KeyCode.D) && !isDefending)
        {
            dirX = vel;
            rb2.velocity = new Vector2(vel, rb2.velocity.y);
        }
        else
        {
            dirX = 0;
            rb2.velocity = new Vector2(0f, rb2.velocity.y);
        }
        if (Input.GetKeyDown(KeyCode.W) && isGrounded && !isDefending)
        {            
            rb2.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
            isGrounded = false;
            print("no puedo saltar");
        }
        if (Input.GetKey(KeyCode.J))           
        {
            isDefending = true;
            this.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
        }
        else
        {
            isDefending = false;
            this.GetComponent<Renderer>().material.SetColor("_Color", Color.white);
        }
        if (Input.GetKeyDown(KeyCode.K) && !isDefending)
        {
            anim.SetBool("isAttacking", true);
            StartCoroutine(Combo("K"));
        }
        else
        {
            anim.SetBool("isAttacking", false);
        }
        if (Input.GetKeyDown(KeyCode.L) && !isDefending)
        {
            anim.SetBool("isThrowing", true);
            StartCoroutine(Combo("L"));
            if (facingRight)
            {
                GameObject newKunai = Instantiate(kunai); //instancio un nuevo kunai
                newKunai.transform.position = new Vector2(rb2.position.x + 1f, rb2.position.y);
                newKunai.GetComponent<Rigidbody2D>().velocity = new Vector2(20f, 2f);
            }
            else
            {
                GameObject newKunai = Instantiate(kunai); 
                newKunai.transform.position = new Vector2(rb2.position.x - 1f, rb2.position.y);
                newKunai.GetComponent<Rigidbody2D>().velocity = new Vector2(-20f, 2f);
                newKunai.transform.localScale = new Vector2(-1f, 1f);
            }
            kunaiDamage = 5;
        }
        else
        {
            anim.SetBool("isThrowing", false);
        }
    }

    //corrutina combo
    IEnumerator Combo(string keyIn)
    {
        if (keyIn == "K")
        {
            if (comboStatus == 0)
            {
                comboStatus = 1;
                yield return new WaitForSeconds(0.5f);
                if (comboStatus == 1)
                {
                    comboStatus = 0;
                }
            }
            else
            {
                comboStatus = 0;
            }
        }
        else if (keyIn == "L")
        {
            if (comboStatus == 1)
            {
                kunaiDamage = kunaiDamage * 20;
                comboStatus = 0;
            }
            else
            {
                comboStatus = 0;
            }
        }
        else
        {
            comboStatus = 0;
        }
    }

    //animaciones
    void SetAnimationState()
    {
        if (dirX == 0){
            anim.SetBool("isRunning", false);
        }
        if (rb2.velocity.y == 0)
        {
            anim.SetBool("isJumping", false);
        }
        if ((dirX == 5 || dirX == -5) && rb2.velocity.y == 0)
        {
            anim.SetBool("isRunning", true);
        }
        else
        {
            anim.SetBool("isRunning", false);
        }
        if (rb2.velocity.y > 0)
        {
            anim.SetBool("isJumping", true);
        }
        if (rb2.velocity.y < 0)
        {
            anim.SetBool("isJumping", false);
        }
    }
}
