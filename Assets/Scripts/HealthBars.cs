﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBars : MonoBehaviour
{
    public PlayerMovement player;
    public Enemy enemy;
    public Slider hpbar;
    public int id;

    private void OnEnable()
    {
        PlayerMovement.OnDamaged += HandleOnDamaged;
        Enemy.OnDamaged += HandleOnDamaged;
    }
    private void OnDisable()
    {
        PlayerMovement.OnDamaged -= HandleOnDamaged;
        Enemy.OnDamaged -= HandleOnDamaged;
    }
    void HandleOnDamaged(int _id, float dmg)
    {
        if (_id == id)
        {
            hpbar.value -= dmg;
        }
        
        
        Debug.Log("a");
    }
}
