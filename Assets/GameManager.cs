using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject level1Btn; //referencia al botón del nivel 1
    public GameObject level2Btn; //referencia al botón del nivel 2

    // Start is called before the first frame update
    void Start()
    {
        level1Btn.SetActive(true); //activo el botón del nivel 1
        level2Btn.SetActive(true); //activo el botón del nivel 1
    }

    // carga nivel 1
    public void Level1()
    {
        SceneManager.LoadScene("Level 1"); //cargo la escena 1
    }
    // carga nivel 2
    public void Level2()
    {
        SceneManager.LoadScene("Level 2"); //cargo la escena 2
    }
}
